# install the dependencies with npm & typings
`$ npm install`
`$ typings install`

# start the server
`$ npm start`
```
go to [http://localhost:8080](http://localhost:8080) in your browser.
```
