import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {State, Appointment} from '../shared/stateTypes';
import {addAppointment, removeAppointment, updateAppointment, setSelectedWeek} from '../shared/actions';
import * as moment from 'moment';

@Injectable()
export class CalendarModel {
  public weekOverviews$ = this.store.select(state => state.data.weekOverviews);
  public selectedWeek$ = this.store.select(state => state.application.selectedWeek);

  constructor(private store: Store<State>) {
  }

  public resetCalendar(): void {
    let time = moment();
    this.store.dispatch(setSelectedWeek(time.isoWeek(), time.isoWeekYear()));
  }

  public gotoYearWeek(year: number, week: number): void {
    this.store.dispatch(setSelectedWeek(week, year));
  }

  public previous(): void {
    let curWeek = this.store.getState().application.selectedWeek;
    let calculatedTime = moment().year(curWeek.year).week(curWeek.week).startOf('week').add(-1, 'weeks');
    this.store.dispatch(setSelectedWeek(calculatedTime.week(), calculatedTime.year()));
  }

  public next(): void {
    let curWeek = this.store.getState().application.selectedWeek;
    let calculatedTime = moment().year(curWeek.year).week(curWeek.week).startOf('week').add(1, 'weeks');
    this.store.dispatch(setSelectedWeek(calculatedTime.week(), calculatedTime.year()));
  }

  public addAppointment(appointment: Appointment): void {
    let aM = moment(appointment.date);
    let day = {year: aM.year(), month: aM.month(), day: aM.date(), day_name: aM.format('dddd')};
    this.store.dispatch(addAppointment(appointment, day));
  }

  public removeAppointment(appointment: Appointment): void {
    let aM = moment(appointment.date);
    let day = {year: aM.year(), month: aM.month(), day: aM.date(), day_name: aM.format('dddd')};
    this.store.dispatch(removeAppointment(appointment.id, day));
  }

  public updateAppointment(appointment: Appointment): void {
    let aM = moment(appointment.date);
    let day = {year: aM.year(), month: aM.month(), day: aM.date(), day_name: aM.format('dddd')};
    this.store.dispatch(updateAppointment(appointment, day));
  }
}
