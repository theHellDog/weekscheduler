import { provideRouter, RouterConfig } from '@angular/router';

import { CalendarComponent } from './calendar';

export const routes: RouterConfig = [
  { path: '', component: CalendarComponent },
  { path: 'year/:year/week/:week', component: CalendarComponent }
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
