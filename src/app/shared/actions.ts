import {Action} from '@ngrx/store';
import {Appointment, Day} from './stateTypes';
// import {MonthOfYear} from './enums';

export const SET_SELECTEDWEEK = 'application:SET_SELECTEDWEEK';

export const ADD_APPOINTMENT = 'data:months:ADD_APPOINTMENT';
export const REMOVE_APPOINTMENT = 'data:months:REMOVE_APPOINTMENT';
export const UPDATE_APPOINTMENT = 'data:months:UPDATE_APPOINTMENT';

export function addAppointment(appointment: Appointment, day: Day): Action {
  return {
    type: ADD_APPOINTMENT,
    payload: {appointment, day}
  };
}

export function removeAppointment(id: string, day: Day): Action {
  return {
    type: REMOVE_APPOINTMENT,
    payload: {id, day}
  };
}

export function updateAppointment(appointment: Appointment, day: Day): Action {
  return {
    type: UPDATE_APPOINTMENT,
    payload: {appointment, day}
  };
}

export function setSelectedWeek(week: number, year: number): Action {
  return {
    type: SET_SELECTEDWEEK,
    payload: {week, year}
  };
}
