import {Action} from '@ngrx/store';
import {DataState, ApplicationState, WeekOverview, DayWithAppointments, Appointment} from './stateTypes';
import {SET_SELECTEDWEEK, ADD_APPOINTMENT, UPDATE_APPOINTMENT, REMOVE_APPOINTMENT} from './actions';

export const store = {
  data: dataReducer,
  application: applicationReducer
};

function dataReducer(state: DataState = {weekOverviews: []}, action: Action): DataState {
  switch (action.type) {
    case ADD_APPOINTMENT:
    case REMOVE_APPOINTMENT:
    case UPDATE_APPOINTMENT:
      return {
        weekOverviews: WeekOverviewsReducer(state.weekOverviews, action)
      };
    default:
      return state;
  }
}

// handles the array of months, if a single action should be taken on a month, it delegates the action to the montOverviewReducer
function WeekOverviewsReducer(state: Array<WeekOverview> = [], action: Action): Array<WeekOverview> {
  switch (action.type) {
    case ADD_APPOINTMENT:
    case UPDATE_APPOINTMENT:
    case REMOVE_APPOINTMENT:
      let found = false;
      let output: Array<WeekOverview> = state.map((WeekOverview: WeekOverview) => {
        if (WeekOverview.week === action.payload.day.week && WeekOverview.year === action.payload.day.year) {
          found = true;
          return WeekOverviewReducer(WeekOverview, action);
        }
        return WeekOverview;
      });
      // if it's not found, we must create a new one
      if (!found) {
        output = [...state,
          WeekOverviewReducer(
            {
              year: action.payload.day.year,
              week: action.payload.day.week,
              daysWithAppointments: []
            },
            action
          )
        ];
      }
      return output;
    default:
      return state;
  }
}

function WeekOverviewReducer(state: WeekOverview, action: Action): WeekOverview {
  switch (action.type) {
    case ADD_APPOINTMENT:
    case UPDATE_APPOINTMENT:
    case REMOVE_APPOINTMENT:
      return {
        year: state.year,
        week: state.week,
        daysWithAppointments: dayWithAppointmentsReducer(state.daysWithAppointments, action)
      };
    default:
      return state;
  }
}

function dayWithAppointmentsReducer(state: Array<DayWithAppointments> = [], action: Action): Array<DayWithAppointments> {
  switch (action.type) {
    case REMOVE_APPOINTMENT:
    case ADD_APPOINTMENT:
    case UPDATE_APPOINTMENT:
      let found = false;
      let output: Array<DayWithAppointments> = state.map((dayWithAppointments: DayWithAppointments) => {
        if (dayWithAppointments.day.day === action.payload.day.day) {
          found = true;
          return dayWithAppointmentReducer(dayWithAppointments, action);
        }
        return dayWithAppointments;
      });
      if (!found) {
        output = [...state,
          dayWithAppointmentReducer(
            {
              day: action.payload.day,
              appointments: []
            },
            action
          )
        ];
      }
      return output;
    default:
      return state;
  }
}

// every change to the events for a day are handled here.
function dayWithAppointmentReducer(state: DayWithAppointments, action: Action): DayWithAppointments {
  switch (action.type) {
    case REMOVE_APPOINTMENT:
      return {
        day: state.day,
        appointments: state.appointments.filter((appointment: Appointment) => {
          return appointment.id !== action.payload.id;
        })
      };
    case ADD_APPOINTMENT:
      return {
        day: state.day,
        appointments: [...state.appointments, action.payload.appointment]
      };
    case UPDATE_APPOINTMENT:
      return {
        day: state.day,
        appointments: state.appointments.map((appointment: Appointment) => {
          let {description, date} = action.payload.appointment;
          return appointment.id === action.payload.appointment.id ?
            Object.assign({}, appointment, {description, date}) : appointment;
        })
      };
    default:
      return state;
  }
}

function applicationReducer(state: ApplicationState = {selectedDay: null, selectedWeek: null, selectedMonth: null},
  action: Action): ApplicationState {
  switch (action.type) {
    case SET_SELECTEDWEEK:
      return Object.assign({}, state, {selectedWeek: action.payload});
    default:
      return state;
  }
}
