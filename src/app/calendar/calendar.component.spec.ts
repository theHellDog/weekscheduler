import {
  inject,
  addProviders,
} from '@angular/core/testing';

// Load the implementations that should be tested
import { CalendarComponent } from './calendar.component';

describe('Calendar', () => {
  // provide our implementations or mocks to the dependency injector
  beforeEach(() => {
    addProviders([CalendarComponent]);
  });

  it('should log ngOnInit', inject([CalendarComponent], (calendar) => {
    spyOn(console, 'log');
    expect(console.log).not.toHaveBeenCalled();

    calendar.ngOnInit();
    expect(console.log).toHaveBeenCalledWith('Hello Home');
  }));

});
