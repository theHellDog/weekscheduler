import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit, OnDestroy} from '@angular/core';
import {Appointment, DayWithAppointments, WeekOverview, Week} from '../shared/stateTypes';
import {Observable, Subscription} from 'rxjs';
import * as moment from 'moment';
import {DayDetailComponent} from './day-detail.component';

@Component({
  selector: 'my-week-view',
  directives: [DayDetailComponent],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: require('./week-view.component.pug'),
  styleUrls: ['./week-view.component.scss']
})
export class WeekViewComponent implements OnInit, OnDestroy {
  private subscriptions: Array<Subscription> = [];

  @Input() public overview: Observable<WeekOverview>;
  @Input() public week: Observable<Week>;

  @Output() public addAppointment = new EventEmitter<Appointment>();
  @Output() public updateAppointment = new EventEmitter<Appointment>();
  @Output() public removeAppointment = new EventEmitter<Appointment>();

  public hours: Array<string> = [];
  public days$: Observable<Array<DayWithAppointments>>;
  public emptyDaysWithAppointments: Array<DayWithAppointments>;

  public tracker(index: number): number {
    return index;
  }


  constructor() {
    for (let i = 0; i < 24; i++) {
      this.hours.push(moment().hours(i).minutes(0).format('HH:mm'));
    }
  }

  public ngOnInit(): void {
    this.days$ = this.overview.map((overview: WeekOverview) => {
      return this.emptyDaysWithAppointments.map((dayWithAppointments: DayWithAppointments) => {
        return overview.daysWithAppointments.filter((item: DayWithAppointments) => {
          return item.day.day === dayWithAppointments.day.day;
        })[0] || dayWithAppointments;
      });
    });

    this.subscriptions.push(this.week.subscribe((week: Week) => {
      this.emptyDaysWithAppointments = this.getDefaultDaysWithAppointments(week);
    }));
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  private getDefaultDaysWithAppointments(week: Week): Array<DayWithAppointments> {
    let days: Array<DayWithAppointments> = [];
    let momentWeek = moment().isoWeekYear(week.year).isoWeek(week.week);
    for (let i = 1; i <= 7; i++) {
      let fd = momentWeek.startOf('week');
      let date = fd.add(i, 'days');
      days.push({day: {year: date.year(), month: date.month(), day: date.date(), day_name: date.format('dddd')}, appointments: []});
    }
    return days;
  }
}
