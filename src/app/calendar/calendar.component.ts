import {Component, OnInit, OnDestroy} from '@angular/core';
import {ROUTER_DIRECTIVES, ActivatedRoute, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import * as moment from 'moment';

import {Appointment, DayWithAppointments, WeekOverview, Week} from '../shared/stateTypes';
import {WeekViewComponent} from './week-view.component';
import {CalendarModel} from '../models/calendar.model';

@Component({
  selector: 'my-calendar',
  providers: [CalendarModel],
  directives: [ROUTER_DIRECTIVES, WeekViewComponent],
  template: require('./calendar.component.pug'),
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, OnDestroy {
  private subscriptions: Array<Subscription> = [];

  public selectedWeek$ = this.model.selectedWeek$;
  private weekOverviews$ = this.model.weekOverviews$;

  public currentWeekOverview$: Observable<WeekOverview> = Observable.combineLatest(
        this.weekOverviews$, this.selectedWeek$, (weekOverviews: Array<WeekOverview>, selectedWeek: Week) => {
            let daysWithAppointments = [];
            let weekMoment = moment().year(selectedWeek.year).week(selectedWeek.week);
            weekOverviews.forEach((overview: WeekOverview) => {
                let matchingDays = overview.daysWithAppointments.filter((day: DayWithAppointments) => {
                    let dayMoment = moment().year(day.day.year).month(day.day.month).date(day.day.day);
                    return dayMoment > weekMoment.startOf('week') && dayMoment < weekMoment.endOf('week');
                });
                daysWithAppointments.push(...matchingDays);
            });
            return new WeekOverview(selectedWeek.year, selectedWeek.week, daysWithAppointments);
        });

  constructor(private model: CalendarModel, private router: Router, private route: ActivatedRoute) {
  }

  public ngOnInit(): void {
    this.subscriptions.push(this.route.params.subscribe(params => {
      let year = +params['year'];
      let week = +params['week'];
      if (year && week) {
        this.model.gotoYearWeek(year, week);
      } else {
        this.model.resetCalendar();
      }
    }));
    this.subscriptions.push(this.selectedWeek$.subscribe(selectedWeek => {
      let link = ['/year', selectedWeek.year, '/week', selectedWeek.week];
      this.router.navigate(link);
    }));
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  public previous(): void {
    this.model.previous();
  }

  public next(): void {
    this.model.next();
  }

  public addAppointment(appointment: Appointment): void {
    this.model.addAppointment(appointment);
  }

  public removeAppointment(appointment: Appointment): void {
    this.model.removeAppointment(appointment);
  }

  public updateAppointment(appointment: Appointment): void {
    this.model.updateAppointment(appointment);
  }
}
