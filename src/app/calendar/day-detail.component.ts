import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit} from '@angular/core';
import {Appointment, DayWithAppointments} from '../shared/stateTypes';
import * as  moment from 'moment';
// import {AppointmentDetail} from './appointment-detail.component';
@Component({
  selector: 'my-day-detail',
  // directives: [AppointmentDetail],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: require('./day-detail.component.pug'),
  styleUrls: ['./day-detail.component.scss']
})
export class DayDetailComponent implements OnInit {
  @Input() public dayWithAppointments: DayWithAppointments;

  @Output() public addAppointment = new EventEmitter<Appointment>();
  @Output() public updateAppointment = new EventEmitter<Appointment>();
  @Output() public removeAppointment = new EventEmitter<Appointment>();

  public onAdd(): void {
    let fakeDate = moment().year(this.dayWithAppointments.day.year).month(this.dayWithAppointments.day.month)
      .date(this.dayWithAppointments.day.day).hours(0).minutes(0);
    this.addAppointment.emit(new Appointment(fakeDate.toDate(), ''));
  }

  constructor() {
  }

  public ngOnInit(): void {
    this.onAdd();
  }
}
