describe('Calendar', function () {

  beforeEach(function () {
    browser.get('/');
  });

  it('should have <my-calendar>', function () {
    var home = element(by.css('my-app my-calendar'));
    expect(home.isPresent()).toEqual(true);
  });

});
