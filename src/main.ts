import { enableProdMode } from '@angular/core';
import { bootstrap } from '@angular/platform-browser-dynamic';
import { HTTP_PROVIDERS } from '@angular/http';
import {provideStore, usePostMiddleware, usePreMiddleware, Middleware} from '@ngrx/store';
import 'rxjs/add/operator/do';

import { AppComponent } from './app/app.component';
import { APP_ROUTER_PROVIDERS } from './app/app.routes';
import {store} from './app/shared/store';

// depending on the env mode, enable prod mode or add debugging modules
if (process.env.ENV === 'build') {
  enableProdMode();
}

const actionLog: Middleware = (action: any) => {
  return action.do((val: any) => {
    console.warn('DISPATCHED ACTION: ', val);
  });
};
const stateLog: Middleware = (state: any) => {
  return state.do((val: any) => {
    console.log('NEW STATE: ', val);
  });
};

bootstrap(AppComponent, [
    // These are dependencies of our App
    HTTP_PROVIDERS,
    APP_ROUTER_PROVIDERS,
    provideStore(store),
    usePreMiddleware(actionLog),
    usePostMiddleware(stateLog)
  ])
  .catch(err => console.error(err));
